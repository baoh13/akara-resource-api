﻿using Akara.Resource.Application.Common.Models;
using Akara.Resource.Infrastructure.Services;
using Microsoft.AspNetCore.Mvc;

namespace Akara.Resource.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ReviewsController : ControllerBase
    {
        private readonly IReviewService _reviewService;

        public ReviewsController(IReviewService reviewService)
        {
            _reviewService = reviewService;
        }

        [HttpPost]
        public IActionResult Add(Review review)
        {
            _reviewService.AddReview(review);

            return Ok(review);
        }

        [HttpPatch]
        public IActionResult Update(Review review)
        {
            var searchedReview = _reviewService.Get(review.Id);

            if (searchedReview == null)
                return BadRequest($"Review {review.Id} doesnt not exist");

            if (searchedReview.UseNewFeatures != review.UseNewFeatures)
            {
                var updatedReview = _reviewService.UpdateReview(review);
                return Ok(updatedReview);
            }
            
            return Ok(review);
        }

        [HttpGet]
        [Route("{id}")]
        public IActionResult GetById(long id)
        {
            var review = _reviewService.Get(id);

            if (review == null)
            {
                return BadRequest("Invalid Id");
            }

            return Ok(review);
        }

        [HttpGet]
        public IActionResult GetByEventId([FromQuery] long eventId)
        {
            var reviews = _reviewService.GetByEventId(eventId);
            return Ok(reviews);
        }
    }
}
