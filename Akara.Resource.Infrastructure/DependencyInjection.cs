﻿using Akara.Resource.Infrastructure.Persistence;
using Akara.Resource.Infrastructure.Persistence.Repositories;
using Akara.Resource.Infrastructure.Services;
using AutoMapper;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System.Reflection;

namespace Akara.Resource.Infrastructure
{
    public static class DependencyInjection
    {
        public static IServiceCollection AddInfrastructure(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddDbContext<ApplicationDbContext>(
                options => options.UseSqlServer(configuration.GetConnectionString("Default")));

            services.AddAutoMapper(Assembly.GetExecutingAssembly());

            services.AddTransient<IEventRepository, EventRepository>();
            services.AddTransient<IReviewRepository, ReviewRepository>();
            services.AddTransient<IEventServices, EventServices>();
            services.AddTransient<IReviewService, ReviewService>();

            return services;
        }
    }
}
