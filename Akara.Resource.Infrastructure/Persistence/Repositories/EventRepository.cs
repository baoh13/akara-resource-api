﻿using Akara.Resource.Application.Common.Models;
using System.Collections.Generic;
using System.Linq;
using Microsoft.EntityFrameworkCore;

namespace Akara.Resource.Infrastructure.Persistence.Repositories
{
    public class EventRepository: IEventRepository
    {
        private readonly ApplicationDbContext _dbContext;

        public EventRepository(ApplicationDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public IEnumerable<Event> GetEvents()
        {
            var events = _dbContext.Events
                                   .Include(e => e.Reviews)
                                   .OrderBy(e => e.FeaturedDate);
            return events;
        }
    }
}
