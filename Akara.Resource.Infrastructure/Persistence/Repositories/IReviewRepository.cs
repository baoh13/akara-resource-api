﻿using Akara.Resource.Application.Common.Models;
using System.Collections.Generic;

namespace Akara.Resource.Infrastructure.Persistence.Repositories
{
    public interface IReviewRepository
    {
        void Add(Review review);
        Review Update(Review review);
        Review Get(long id);
        IEnumerable<Review> GetByEventId(long eventId);
    }
}
