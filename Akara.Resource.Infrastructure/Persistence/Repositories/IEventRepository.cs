﻿using Akara.Resource.Application.Common.Models;
using System.Collections.Generic;

namespace Akara.Resource.Infrastructure.Persistence.Repositories
{
    public interface IEventRepository
    {
        IEnumerable<Event> GetEvents();
    }
}