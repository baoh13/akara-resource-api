﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Akara.Resource.Application.StorageAccount
{
    public class StorageAccountOptions
    {
        public string Name { get; set; }
        public string ConnectionString { get; set; }
        public string AccountKey { get; set; }
        public string FullImagesContainerName { get; set; }
    }
}
