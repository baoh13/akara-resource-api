﻿using Akara.Resource.Application.Common.Models;
using Akara.Resource.Infrastructure.Persistence.Repositories;
using System;
using System.Collections.Generic;

namespace Akara.Resource.Infrastructure.Services
{
    public class ReviewService : IReviewService
    {
        private readonly IReviewRepository _reviewRepository;

        public ReviewService(IReviewRepository reviewRepository)
        {
            _reviewRepository = reviewRepository;
        }

        public void AddReview(Review review)
        {
            if (review == null)
                throw new NullReferenceException(nameof(review));

            _reviewRepository.Add(review);
        }

        public Review Get(long id)
        {
            return _reviewRepository.Get(id);
        }

        public IEnumerable<Review> GetByEventId(long id)
        {
            var reviews = _reviewRepository.GetByEventId(id);
            return reviews;
        }

        public Review UpdateReview(Review review)
        {
            if (review == null)
                throw new NullReferenceException(nameof(review));

            var updatedReview = _reviewRepository.Update(review);

            return updatedReview;
        }
    }
}
