﻿using Akara.Resource.Application.Common.Models;
using Akara.Resource.Infrastructure.Mappings;
using AutoMapper;
using System;

namespace Akara.Resource.Infrastructure.Dtos
{
    public class EventDto : IMapFrom<Event>
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public DateTime Date { get; set; }
        public DateTime FeaturedDate { get; set; }
        public string Address { get; set; }
        public string City { get; set; }
        public string Country { get; set; }
        public string About { get; set; }
        public int NumberOfAttendees { get; set; }
        public string Features { get; set; }
        public string Industry { get; set; }
        public string Topics { get; set; }
        public string Email { get; set; }
        public string Website { get; set; }
        public string Images { get; set; }
        public string Organizer { get; set; }
        public double OverallRating { get; set; }

        public void Mapping(Profile profile) { }
    }
}
