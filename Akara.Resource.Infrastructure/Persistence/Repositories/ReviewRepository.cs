﻿using Akara.Resource.Application.Common.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Akara.Resource.Infrastructure.Persistence.Repositories
{
    public class ReviewRepository : IReviewRepository
    {
        private readonly ApplicationDbContext _applicationDbContext;
        public ReviewRepository(ApplicationDbContext applicationDbContext)
        {
            _applicationDbContext = applicationDbContext;
        }

        public void Add(Review review)
        {
            if (review == null)
                throw new NullReferenceException(nameof(review));
            
            review.DateCreated = DateTime.UtcNow;

            _applicationDbContext.Reviews.Add(review);
            _applicationDbContext.SaveChanges();            
        }

        public Review Get(long id)
        {
            return _applicationDbContext.Reviews.SingleOrDefault(r => r.Id == id);
        }

        public IEnumerable<Review> GetByEventId(long eventId)
        {
            return _applicationDbContext.Reviews
                                        .Where(r => r.EventId == eventId)
                                        .OrderByDescending(r => r.DateCreated);
        }

        public Review Update(Review review)
        {
            var searchedReview = _applicationDbContext.Reviews.SingleOrDefault(r => r.Id == review.Id);

            if (searchedReview != null & searchedReview.UseNewFeatures != review.UseNewFeatures)
            {
                searchedReview.UseNewFeatures = review.UseNewFeatures;
                _applicationDbContext.SaveChanges();
                return searchedReview;
            }

            return review;
        }
    }
}
