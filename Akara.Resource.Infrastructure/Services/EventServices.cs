﻿using Akara.Resource.Application.Common.Models;
using Akara.Resource.Infrastructure.Dtos;
using Akara.Resource.Infrastructure.Persistence.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Akara.Resource.Infrastructure.Services
{
    public class EventServices : IEventServices
    {
        private readonly IEventRepository _eventRepository;

        public EventServices(IEventRepository eventRepository)
        {
            _eventRepository = eventRepository;
        }

        public IList<EventDto> GetEvents()
        {
            var events = _eventRepository.GetEvents().ToList();

            var result = new List<EventDto>();

            events.ForEach(e => {
                var eventDto = MapToDto(e);
                result.Add(eventDto);
            });

            return result;
        }

        private EventDto MapToDto(Event e)
        {
            var dto = new EventDto()
            {
                Id = e.Id,
                About = e.About,
                Address = e.Address,
                City = e.City,
                Country = e.Country,
                Date = e.Date,
                Email = e.Email,
                FeaturedDate = e.FeaturedDate,
                Features = e.Features,
                Images = e.Images,
                Industry = e.Industry,
                Name = e.Name,
                NumberOfAttendees = e.NumberOfAttendees,
                Organizer = e.Organizer,
                Topics = e.Topics,
                Website = e.Website,
                OverallRating = e.Reviews.Any() ? GetEventOverallRating(e) : 0           
            };

            return dto;
        }

        private int GetEventOverallRating(Event e)
        {
            var reviews = e.Reviews;

            var overallRating = (int)Math.Floor((double)(reviews.Sum(r => r.OverallRating)/reviews.Count()) + 0.5);

            return overallRating;
        }
    }
}
