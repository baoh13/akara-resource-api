﻿using Akara.Resource.Infrastructure.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Linq;

namespace Akara.Resource.Api.Controllers
{
    [Authorize(Policy = "ApiReader")]
    [Route("api/[controller]")] 
    [ApiController]
    public class ValuesController : ControllerBase
    {
        private readonly IEventServices _eventServices;

        public ValuesController(IEventServices eventServices)
        {
            _eventServices = eventServices;
        }

        [Authorize(Policy = "Consumer")]
        [HttpGet]
        public IActionResult Get()
        {
            return new JsonResult(User.Claims.Select(c => new { c.Type, c.Value }));
        }
    }
}
