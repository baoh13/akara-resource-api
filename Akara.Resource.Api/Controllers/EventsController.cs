﻿using Akara.Resource.Infrastructure.Services;
using Microsoft.AspNetCore.Mvc;

namespace Akara.Resource.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class EventsController : ControllerBase
    {
        private readonly IEventServices _eventServices;

        public EventsController(IEventServices eventServices)
        {
            _eventServices = eventServices;
        }

        [HttpGet]
        public IActionResult GetEvents()
        {
            var events = _eventServices.GetEvents();

            if (events == null)
                return BadRequest();

            return Ok(events);
        }
    }
}
