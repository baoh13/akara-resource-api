﻿using AutoMapper;

namespace Akara.Resource.Infrastructure.Mappings
{
    public interface IMapFrom<T>
    {
        void Mapping(Profile profile) => profile.CreateMap(typeof(T), GetType());
    }
}
