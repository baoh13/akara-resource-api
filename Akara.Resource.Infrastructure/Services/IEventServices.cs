﻿using Akara.Resource.Infrastructure.Dtos;
using System.Collections.Generic;

namespace Akara.Resource.Infrastructure.Services
{
    public interface IEventServices
    {
        IList<EventDto> GetEvents();
    }
}
