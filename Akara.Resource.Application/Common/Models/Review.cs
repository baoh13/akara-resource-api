﻿using System;

namespace Akara.Resource.Application.Common.Models
{
    public class Review
    {
        public long Id { get; set; }
        public long EventId { get; set; }
        public Event Event { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string NetworkingOpportunities { get; set; }
        public string QualityOfBusinessMeetings { get; set; }
        public string QualityOfSpeakersAndContent { get; set; }
        public string SeniorityOfAttendees { get; set; }
        public string ValueForMoneyAndTime { get; set; }
        public bool AbleToContact { get; set; } //AgreedTerms&Conditions
        public bool IsNotCompetitor { get; set; }
        public string DelegateType { get; set; }
        public string Organization { get; set; }
        public int OverallRating { get; set; }
        public bool Recommend { get; set; }
        public bool UseFirstName { get; set; } 
        public bool UseNewFeatures { get; set; } // 1-1
        public string ReviewHeadline { get; set; }
        public string ReviewText { get; set; }
        public string Email { get; set; }
        public DateTime? DateCreated { get; set; }
    }
}
