﻿using Akara.Resource.Application.Common.Models;
using System.Collections.Generic;

namespace Akara.Resource.Infrastructure.Services
{
    public interface IReviewService
    {
        void AddReview(Review review);
        Review UpdateReview(Review review);
        Review Get(long id);
        IEnumerable<Review> GetByEventId(long id);
    }
}
